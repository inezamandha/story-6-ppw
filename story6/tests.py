from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .views import *
from .models import Status
from .forms import StatusForm
from datetime import date
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story6Test(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_using_story_6_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_using_story_6_view(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_content_in_landingpage(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Halo, apa kabar?", html_response)

class Story6_Model_Test(TestCase):
    def test_model_create_new_status(self):
        new_status = Status.objects.create(date=timezone.now(), status='Butuh liburan :(')

        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)

    def test_status_url_is_exist(self):
        response = Client().get('/add_status')
        self.assertEqual(response.status_code, 301)

    def test_save_POST_request_to_save_status(self):
        response = self.client.post('/add_status/', data={'date' : '2019-11-03T20:00', 'status' : 'Butuh liburan :('})
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object, 1)

        self.assertEqual(response.status_code, 302)

        new_response = self.client.get('/status/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Butuh liburan :(', html_response)

class Story6_Form_Test(TestCase):
    def test_form_valid(self):
        form_data = {'status': 'Butuh liburan :('}
        form = StatusForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_form_not_valid(self):
        form_data = {'status': 'Butuh liburan banget! Rasanya sudah mumet di semester ini tapi harus tetap semangat'*100}
        form = StatusForm(data = form_data)
        self.assertFalse(form.is_valid())

    def test_form_blank_not_valid(self):
        form_data = {'status': ''}
        form = StatusForm(data = form_data)
        self.assertFalse(form.is_valid())

class Story6_Functional_Test(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6_Functional_Test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story6_Functional_Test, self).tearDown()

    def test_element_in_landing_page(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/status/')
        time.sleep(5)

        hello = selenium.find_element_by_tag_name('h1')
        self.assertEqual(selenium.find_element_by_id('hello'), hello)

    def test_input_status_form(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/status/')
        time.sleep(5)

        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        status_message = 'Coba coba'
        status.send_keys(status_message)
        submit.click()
        time.sleep(5)
        self.assertIn(status_message, selenium.page_source)

    def test_change_theme(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/status/')
        time.sleep(5)

        button = selenium.find_element_by_id('theme')
        button.click()

        test_1 = selenium.find_element_by_class_name('tanya-kabar')

        background_tanyakabar = test_1.value_of_css_property('background-image')
        self.assertEqual(background_tanyakabar, 'url("http://127.0.0.1:8000/static/images/background-2.jpg")')

        test_11 = test_1.find_element_by_tag_name('h1')
        warna_judul_tanyakabar = test_11.value_of_css_property('color')
        self.assertEqual(warna_judul_tanyakabar, 'rgba(22, 96, 136, 1)')

        test_12 = test_1.find_element_by_tag_name('label')
        warna_huruf_tanyakabar = test_12.value_of_css_property('color')
        self.assertEqual(warna_huruf_tanyakabar, 'rgba(2, 50, 70, 1)')

        test_2 = selenium.find_element_by_class_name('data-kabar')

        background_datakabar = test_2.value_of_css_property('background-color')
        self.assertEqual(background_datakabar, 'rgba(74, 111, 165, 1)')

        test_21 = test_2.find_element_by_tag_name('h1')
        warna_judul_datakabar = test_21.value_of_css_property('color')
        self.assertEqual(warna_judul_datakabar, 'rgba(245, 249, 249, 1)')

        test_22 = test_2.find_element_by_tag_name('div')
        test_22_1 = test_22.find_element_by_id('curhatan')
        warna_kotak_datakabar = test_22_1.value_of_css_property('background-color')
        self.assertEqual(warna_kotak_datakabar, 'rgba(245, 249, 249, 1)')

        warna_shadow_kotak_datakabar = test_22_1.value_of_css_property('box-shadow')
        self.assertEqual(warna_shadow_kotak_datakabar, 'rgb(0, 27, 72) 10px 10px 5px 0px')

        test_3 = selenium.find_element_by_tag_name('nav')
        warna_navbar = test_3.value_of_css_property('background-color')
        self.assertEqual(warna_navbar, 'rgba(188, 205, 212, 0.65)')

        test_4 = selenium.find_element_by_tag_name('button')
        warna_button = test_4.value_of_css_property('background-color')
        self.assertEqual(warna_button, 'rgba(22, 96, 136, 1)')