from django.urls import path
from .views import *

app_name = 'story6'

urlpatterns = [
    path('status/', index, name='index'),
    path('add_status/', add_status, name='add_status'),
]