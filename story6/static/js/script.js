$('document').ready(function(){
    var allPanels = $('.panel').hide();
    var active;
    var clicked = false;

    $(document).on('click', '.accordion', function() {
        allPanels.slideUp();
        if(this === active){
        $(this).next().slideUp();
        $(this).removeClass('active');
        active = null;
        return false;
        }
        $(active).removeClass('active');
        $(this).addClass('active');
        $(this).next().slideDown();
        active = this;
        return false;
    });

    $("#theme").click(function(){
        if(clicked) {
            $('.tanya-kabar').css('background-image', "url('/static/images/background.jpg')");
            $('.tanya-kabar h1').css('color', '#F7EEE5');
            $('.tanya-kabar label').css('color', '#F7EEE5');
            $('.tanya-kabar button').css('background-color', '#CB9A7C');
            $('.data-kabar').css('background-color', '#CB9A7C');
            $('.data-kabar h1').css('color', '#F7EEE5');
            $('.data-kabar div #curhatan').css('background-color', '#FFF1E6');
            $('.data-kabar div #curhatan').css('box-shadow', '10px 10px 5px grey');
            $('nav').css('background', 'rgba(240, 227, 219, 0.65)');
            $('nav div a').css('color', '#454416');
            $('nav button').css('background-color', '#CB9A7C');

            $('.hello-page').css('background-image', "url('/static/images/profile-background-1.jpg')")
            $('.about-me-text').css('background-color', '#A96F44');
            $('.about-me-content').css('background-color', '#F2ECB6');
            $('#profile-photo').css('border', '10px solid #CEA47E');
            $('.short-info-box').css('background-color', '#EAC77D');
            $('.resume-text').css('background-color', '#A96F44');
            $('.accordion').css('background-color', '#F2ECB6');
            $('.contact-text').css('background-color', '#A96F44');
            $('.contact-content').css('background-color', '#F2ECB6');
            $('#kotak-kontak').css('background-color', '#EAC77D');
            $('.contact-content div #logo-circle').css('background-color', '#CEA47E');

            $('.cari-buku').css('background-image', "url('/static/images/background-book.jpg')");
            $('.cari-buku h1').css('color', '#F7EEE5');
            $('.cari-buku button').css('background-color', '#CB9A7C');
            $('#hasil-pencarian div table thead').css('background-color', '#A96F44');
            $('#hasil-pencarian div table tbody').css('background-color', '#F2ECB6');

            clicked = false;

        } else {
            $('.tanya-kabar').css('background-image', "url('/static/images/background-2.jpg')");
            $('.tanya-kabar h1').css('color', '#166088');
            $('.tanya-kabar label').css('color', '#023246');
            $('.tanya-kabar button').css('background-color', '#166088');
            $('.data-kabar').css('background-color', '#4A6FA5');
            $('.data-kabar h1').css('color', '#F5F9F9');
            $('.data-kabar div #curhatan').css('background-color', '#F5F9F9');
            $('.data-kabar div #curhatan').css('box-shadow', '10px 10px 5px #001B48');
            $('nav').css('background', 'rgba(188, 205, 212, 0.65)');
            $('nav div a').css('color', '#166088');
            $('nav button').css('background-color', '#166088');

            $('.hello-page').css('background-image', "url('/static/images/profile-background-2.jpg')");
            $('.about-me-text').css('background-color', '#1D3B62');
            $('.about-me-content').css('background-color', '#C2CCD8');
            $('#profile-photo').css('border', '10px solid #5886AF');
            $('.short-info-box').css('background-color', '#87A8C5');
            $('.resume-text').css('background-color', '#1D3B62');
            $('.accordion').css('background-color', '#C2CCD8');
            $('.contact-text').css('background-color', '#1D3B62');
            $('.contact-content').css('background-color', '#C2CCD8');
            $('#kotak-kontak').css('background-color', '#87A8C5');
            $('.contact-content div #logo-circle').css('background-color', '#5886AF');

            $('.cari-buku').css('background-image', "url('/static/images/background-book-2.jpg')");
            $('.cari-buku h1').css('color', '#54504C');
            $('.cari-buku button').css('background-color', '#166088');
            $('#hasil-pencarian div table thead').css('background-color', '#1D3B62');
            $('#hasil-pencarian div table tbody').css('background-color', '#C2CCD8');
            
            clicked = true;
            
        };
        return false;
    });
});