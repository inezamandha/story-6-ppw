from django.shortcuts import render, redirect
from django.http import JsonResponse
import json
import requests

response = {}
BOOKS_API = 'https://www.googleapis.com/books/v1/volumes?q='

# Create your views here.
def getJson(request, query):
	url = BOOKS_API + query
	json_data = json.loads(requests.get(url).text)
	return JsonResponse(json_data)

def books(request):
    return render(request, 'books.html', response)