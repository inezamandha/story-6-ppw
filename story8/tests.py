from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from .views import *
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story8_Books_Page_Test(TestCase):
    def test_url_books_page_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_using_books_page_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_using_books_page_view(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    def test_books_page_content(self):
        request = HttpRequest()
        response = books(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Books', html_response)

    def test_getJson_page_url_is_exist(self):
        response = Client().get('/books/getJson/architecture/')
        self.assertEqual(response.status_code, 200)

    def test_getJson_using_getJson_func(self):
        found = resolve('/books/getJson/architecture/')
        self.assertEqual(found.func, getJson)

class Story8_Functional_Test(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        
        super(Story8_Functional_Test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story8_Functional_Test, self).tearDown()

    def test_search_book(self):
        self.selenium.get('http://127.0.0.1:8000/books/')
        time.sleep(10)
        
        input_form = self.selenium.find_element_by_id('search-box')
        input_form.send_keys('architecture')

        self.selenium.find_element_by_id('search-button').click()

        time.sleep(10)
        hasil = self.selenium.find_element_by_id('hasil-pencarian')
        self.assertIn('architecture', hasil.get_attribute('innerHTML').lower())