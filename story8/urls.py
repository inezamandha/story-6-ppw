from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.books, name='books'),
    path('getJson/<str:query>/', views.getJson, name='getJson'),
]