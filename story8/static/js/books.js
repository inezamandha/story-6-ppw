$(document).ready(function(){
	$('#search-button').click(function(){
		var query = $('#search-box').val();

		$(function(){
			$.ajax({
				url: "/books/getJson/" + query,
				success: function(result){
					$('#books-body').empty();		
				    $.each(result.items, function(i, item) {
				        $('<tr>').append(
				        	$('<th scope="row">').text((i+1)),
				        	$('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
				            $('<td>').text(item.volumeInfo.title),
				            $('<td>').text(item.volumeInfo.authors),
				            $('<td>').text(item.volumeInfo.publisher),
							$('<td>').text(item.volumeInfo.publishedDate),
							$('<td>').append('<a href="' + item.volumeInfo.canonicalVolumeLink + '" target="_blank">HERE</a>')
				        ).appendTo('#books-body');
					});
				}
			});
		});
	});
	$('#search-box').val("architecture");
	$('#search-button').click();
});