from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User
from django.urls import reverse
from selenium.webdriver.common.keys import Keys

# Create your tests here.
class Story9_Login_Test(TestCase):
    def test_login_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_using_login_view(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginView)

    def test_logout_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_logout_template(self):
        response = Client().get('/logout/')
        self.assertTemplateUsed(response, 'logout.html')

    def test_using_logout_view(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logoutView)

    def test_registered_is_exist(self):
        response = Client().get('/registered/')
        self.assertEqual(response.status_code, 200)

    def test_registered_template(self):
        response = Client().get('/registered/')
        self.assertTemplateUsed(response, 'registered.html')

    def test_using_registered_view(self):
        found = resolve('/registered/')
        self.assertEqual(found.func, registered)

class Story9_Functional_Test(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story9_Functional_Test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story9_Functional_Test, self).tearDown()

    def test_hello_element_in_project(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)

        hello = selenium.find_element_by_class_name('hello-page')
        hello_text = hello.find_element_by_tag_name('h1').text
        self.assertIn("✿ hello! i'm inez ✿", hello_text)
        time.sleep(10)

    '''
    def test_user_login_page(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/login/')
        time.sleep(5)

        test_1 = selenium.find_element_by_class_name('container')
        test_11 = test_1.find_element_by_tag_name('div')
        test_111 = test_11.find_element_by_tag_name('form')
        test_1111 = test_111.find_element_by_tag_name('h1')
        self.assertIn("Sign In", test_1111.text)
        time.sleep(10)
    
    def test_user_can_login(self):
        # Create user
        User.objects.create_user(first_name='Inez', last_name='Amandha', username='inez.amandha', email='inezamandhasuci@gmail.com', password='hello123')

        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/login/')

        username = selenium.find_element_by_id('username_login')
        password = selenium.find_element_by_id('password_login')
        submit = selenium.find_element_by_id('submit_login')

        username.send_keys('inez.amandha')
        password.send_keys('hello123')
        submit.click()
        time.sleep(10)

        welcome = selenium.find_element_by_tag_name('nav')
        welcome_text = welcome.find_element_by_tag_name('p').text
        self.assertIn("Welcome, Inez Amandha👋", welcome_text)
        time.sleep(10)

    def test_user_can_signup(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/login/')

        test = selenium.find_element_by_class_name('form-container')
        signup = test.find_element_by_class_name('sign-up-container')

        firstname = signupfind_element_by_id('first-name_login')
        lastname = signup.find_element_by_id('last-name_login')
        username = signup.find_element_by_id('username_login')
        email = signup.find_element_by_id('email_login')
        password = signup.find_element_by_id('password_login')
        submit = signup.find_element_by_id('submit_login')

        signup.click()
        firstname.send_keys('Inez')
        lastname.send_keys('Amandha')
        username.send_keys('inez.amandha')
        email.send_keys('inezamandhasuci@gmail.com')
        password.send_keys('hello123')
        submit.click()
        time.sleep(10)

        welcome = selenium.find_element_by_tag_name('nav')
        welcome_text = welcome.find_element_by_tag_name('p').text
        self.assertIn("Welcome, User! 👋", welcome_text)
        time.sleep(10)

    def test_login_with_wrong_username(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/login/')

        username = selenium.find_element_by_id('username_login')
        password = selenium.find_element_by_id('password_login')
        submit = selenium.find_element_by_id('submit_login')

        username.send_keys('inez')
        password.send_keys('hello123')
        submit.click()
        time.sleep(10)

        welcome = selenium.find_element_by_tag_name('nav')
        welcome_text = welcome.find_element_by_tag_name('p').text
        self.assertIn("Welcome, User! 👋", welcome_text)
        time.sleep(10)
    '''