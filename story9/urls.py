from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('login/', views.loginView, name='loginView'),
    path('logout/', views.logoutView, name='logoutView'),
    path('registered/', views.registered, name='registered'),
]