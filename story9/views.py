from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.models import User

from story7.views import *

# Create your views here.

def registered(request):
    template_name = 'registered.html'
    if request.method == "POST":
        if len(request.POST) == 3:
            username_login = request.POST['username']
            password_login = request.POST['password']
                    
            user = authenticate(request, username=username_login, password=password_login)

            if user is not None:
                login(request, user)
                return redirect('story7:profile')
            else:
                return redirect('story9:registered')

    return render(request, template_name)

def loginView(request):
    if request.method == "POST":
        if len(request.POST) == 3:
            username_login = request.POST['username']
            password_login = request.POST['password']
                    
            user = authenticate(request, username=username_login, password=password_login)

            if user is not None:
                login(request, user)
                return redirect('story7:profile')
            else:
                return redirect('story9:loginView')


        elif len(request.POST) == 6:
            firstname_login = request.POST['first-name']
            lastname_login = request.POST['last-name']
            username_login = request.POST['username']
            email_login = request.POST['email']
            password_login = request.POST['password']

            users = User.objects.all()

            for user in users:
                if user.username == username_login :
                    return redirect('story9:registered')

            user = User.objects.create_user(first_name=firstname_login, last_name=lastname_login, username=username_login, email=email_login, password=password_login)
            return redirect('story9:registered')

    return render(request, 'login.html')

def logoutView(request):
    if request.method == "POST":
        logout(request)
        return redirect('story7:profile')

    return render(request, 'logout.html')