from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story7_My_Profile_Test(TestCase):
    def test_url_my_profile_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_my_profile_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'myprofile.html')

    def test_using_my_profile_view(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)

class Story7_Functional_Test(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7_Functional_Test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7_Functional_Test, self).tearDown()

    def test_change_theme_profile(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)

        button = selenium.find_element_by_id('theme')
        button.click()

        test_1 = selenium.find_element_by_class_name('hello-page')

        background_tanyakabar = test_1.value_of_css_property('background-image')
        self.assertEqual(background_tanyakabar, 'url("http://127.0.0.1:8000/static/images/profile-background-2.jpg")')

        test_2 = selenium.find_element_by_class_name('about-me-text')
        
        warna_teks_about_me = test_2.value_of_css_property('background-color')
        self.assertEqual(warna_teks_about_me, 'rgba(29, 59, 98, 1)')

        test_3 = selenium.find_element_by_class_name('about-me-content')

        warna_konten_about_me = test_3.value_of_css_property('background-color')
        self.assertEqual(warna_konten_about_me, 'rgba(194, 204, 216, 1)')

        test_4 = selenium.find_element_by_class_name('short-info-box')
        warna_box = test_4.value_of_css_property('background-color')
        self.assertEqual(warna_box, 'rgba(135, 168, 197, 1)')

        test_5 = selenium.find_element_by_class_name('resume-text')
        warna_teks_resume = test_5.value_of_css_property('background-color')
        self.assertEqual(warna_teks_resume, 'rgba(29, 59, 98, 1)')

        test_6 = selenium.find_element_by_class_name('contact-text')
        warna_teks_contact = test_6.value_of_css_property('background-color')
        self.assertEqual(warna_teks_contact, 'rgba(29, 59, 98, 1)')

        test_7 = selenium.find_element_by_class_name('contact-content')
        warna_konten_contact = test_7.value_of_css_property('background-color')
        self.assertEqual(warna_konten_contact, 'rgba(194, 204, 216, 1)')

        test_8 = selenium.find_element_by_id('kotak-kontak')
        warna_kotak_kontak = test_8.value_of_css_property('background-color')
        self.assertEqual(warna_kotak_kontak, 'rgba(135, 168, 197, 1)')

    def test_accordion(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)

        accordion = selenium.find_element_by_class_name('accordion')
        panel = selenium.find_element_by_class_name('panel')

        tampilan = panel.value_of_css_property('display')
        self.assertEqual(tampilan, 'none')

        accordion.click()

        tampilan = panel.value_of_css_property('display')
        self.assertEqual(tampilan, 'block')