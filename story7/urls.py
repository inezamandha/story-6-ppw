from django.urls import path
from .views import *

app_name = 'story7'

urlpatterns = [
    path('', profile, name='profile'),
]